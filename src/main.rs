use warp::Filter;

#[tokio::main]
async fn main() {
    let hello = warp::path::end().map(|| "hello world!");
    warp::serve(hello).run(([0, 0, 0, 0], 80)).await;
}
